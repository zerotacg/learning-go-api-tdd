package main

import (
	"fmt"
	"gitlab.com/zerotacg/learning-go-api-tdd/internal/app"
	"gitlab.com/zerotacg/learning-go-api-tdd/internal/hello"
)

func main() {
	fmt.Println(hello.Hello("", ""))

	a := app.App{}
	a.Initialize()
	a.Run(":8080")
}
