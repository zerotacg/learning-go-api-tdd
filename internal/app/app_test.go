package app

import (
	"net/http"
	"net/http/httptest"
	"testing"
)

var a App

func TestHelloWorld(t *testing.T) {
	a.Initialize()

	t.Run("simple api test", func(t *testing.T) {
		req, _ := http.NewRequest("GET", "/products", nil)
		response := executeRequest(req)

		checkResponseCode(t, http.StatusOK, response.Code)

		expected := "\"hello, api world\""
		if body := response.Body.String(); body != expected {
			t.Errorf("Expected '%s' Got '%s'", expected, body)
		}
	})
}

// get mock http server for testing proxy
// control mock http server from within test and make proxy calls to this mocked http server

func executeRequest(req *http.Request) *httptest.ResponseRecorder {
	rr := httptest.NewRecorder()
	a.Router.ServeHTTP(rr, req)

	return rr
}
func checkResponseCode(t *testing.T, expected, actual int) {
	if expected != actual {
		t.Errorf("Expected response code %d. Got %d\n", expected, actual)
	}
}
