package hello

import "fmt"

const englishHello = "Hello, %s"
const spanishHello = "Hola, %s"
const frenchHello = "Bonjour, %s"

type Language string

const (
	Default Language = ""
	English          = "English"
	French           = "French"
	Spanish          = "Spanish"
)

func Hello(name string, language string) string {
	if len(name) == 0 {
		name = "World"
	}

	greeting := getGreeting(Language(language))

	return fmt.Sprintf(greeting, name)
}

func getGreeting(language Language) string {
	switch language {
	case English:
		return englishHello
	case French:
		return frenchHello
	case Spanish:
		return spanishHello
	default:
		return englishHello
	}
}
