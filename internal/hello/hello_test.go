package hello

import (
	"testing"
)

func TestHello(t *testing.T) {
	assertEquals := func(t *testing.T, actual string, expected string) {
		t.Helper()

		if actual != expected {
			t.Errorf("actual %q but expected %q", actual, expected)
		}
	}

	t.Run("saying hello to people", func(t *testing.T) {
		assertEquals(t, Hello("Chris", ""), "Hello, Chris")
	})

	t.Run("say 'Hello, World' when an empty string is supplied", func(t *testing.T) {
		assertEquals(t, Hello("", ""), "Hello, World")
	})

	t.Run("in English", func(t *testing.T) {
		assertEquals(t, Hello("Agatha", "English"), "Hello, Agatha")
	})

	t.Run("in Spanish", func(t *testing.T) {
		assertEquals(t, Hello("Elodie", "Spanish"), "Hola, Elodie")
	})

	t.Run("in French", func(t *testing.T) {
		assertEquals(t, Hello("Piere", "French"), "Bonjour, Piere")
	})
}
